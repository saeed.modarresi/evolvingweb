<?php

/**
 * Plugin Name: Wizban Core
 * Plugin URI: https://saeedmodarresi.com
 * Description: This plugin is used for handle the Wizban theme core operations.
 * Author: Saeed.Modarresi
 * Version: 0.0.1
 * Domain Path: /languages
 * Author URI: https://saeedmodarresi.com
 */

defined("ABSPATH") || exit;

/*
|--------------------------------------------------------------------------
| Define root path
|--------------------------------------------------------------------------
*/
if (!defined("DS")) define("DS", DIRECTORY_SEPARATOR);
define("WIZBAN_ABSPATH", plugin_dir_path(__FILE__) . DS);
define("WIZBAN_BASENAME", basename(WIZBAN_ABSPATH));
define("WIZBAN_ROOT_DIR", __FILE__);
define("WIZBAN_ROOT_URL", trailingslashit(plugin_dir_url(__FILE__)));
define("WIZBAN_ROOT_ASSETS", trailingslashit(WIZBAN_ROOT_URL . "assets" . DS));
define("WIZBAN_ROOT_TPL", trailingslashit(WIZBAN_ABSPATH . "templates" . DS));
define("WIZBAN_ROOT_MIGRATION", WIZBAN_ABSPATH . "app\\Database\\Migrations" . DS);
define("WIZBAN_MULTISITES", is_multisite());
define("WIZBAN_API_BEARER_ACCESS_TOKEN_VALID_IN_SECONDS", 86400);
define("WIZBAN_ADMIN_URL", admin_url('admin.php') . "?page=wizban-theme");
define("WIZBAN_CACHE_KEY", 'wizban_response_' . md5(serialize([plugin_basename(WIZBAN_ABSPATH)])));
define("WIZBAN_TRANSLATE_KEY", 'wizban-core-text-domain');
define("WIZBAN_THEME_OPTION_CACHE_KEY", 'theme_option_cache');

/*
|--------------------------------------------------------------------------
| Autoload
|--------------------------------------------------------------------------
*/
require_once __DIR__ . "/vendor/autoload.php";

/*
|--------------------------------------------------------------------------
| Load the plugin's text domain
|--------------------------------------------------------------------------
*/
add_action('plugins_loaded', function () {

    load_plugin_textdomain(WIZBAN_TRANSLATE_KEY, false, dirname(plugin_basename(__FILE__)) . '/languages/');
});

add_filter('admin_body_class', function ($classes) {

    if (isThemeActive()) {
        $classes .= ' ' . esc_attr("wizban-core");
        return $classes;
    }
});

/*
|--------------------------------------------------------------------------
| Check the wizban then create and run application
|--------------------------------------------------------------------------
*/
if (Wizban\Core\App::class) {

    $app = new \Wizban\Core\App();
    $app->init();
}