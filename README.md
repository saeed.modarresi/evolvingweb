# CRM Integration with third party Apps (Development document)

##### Attention:
```
This integration addon should be used for only one type of CRM integration at a time.
Do not use it to integrate with two different types of CRM simultaneously, such as FollowUpBoss and GoHighLevel.
However, you can use it for multiple agents within the same type of CRM, each with different tokens and agent emails.
```
<br>

---

We have a primary CRM integration addon, on which we can install various target integration addons.
For instance, crm-integration-followupboss.

### CRM-Integration Addon Structure:

In this addon, there are two parts:<br>
* 1- The core files for handle the main process:
    * libraries/addon_crm_integration/src/classes
    * libraries/addon_crm_integration/src/traits

* 2- The target addon that inject into types folder
    * libraries/addon_crm_integration/type/followupboss

#### Src folder
In this folder, we keep the main classes
```
1- SourceCRM Class: contain main actions of Source CRM
2- TargetCRM Class: contain main actions of Target CRM
3- BaseApp abstract Class: general methods
4- Some traits like Debug, Target API and etc
``` 

#### Types folder
This is the main folder of each Target CRM with its name.
```
1- Config: all addon's settings are here
2- Api: all target CRM endpoints are here
3- Events: SourceEvents method used in source CRM side(WPL)
4- Events: TargetEvents method used in target CRM side(followupboss webhooks)
``` 
<br>

---



### How the mapping worked?
In our integration system, we require a mapping mechanism to link keys together, forming a readable array for use.

* When inserting data into the Source CRM, use the keys from the Source CRM and the values from the Target CRM.

* When inserting data into the Target CRM, use the keys from the Target CRM and the values from the Source CRM.

Retrieving values from Target CRM with string keys, such as "firstName," is straightforward.For example:
```
Source CRM : first_name
Target CRM : firstName
```

However, in some instances, the keys were different between the GET method and the POST method.

```
Target CRM: (Get method) ["emails","0","value"]
Target CRM: (Post method) emails
```
To handle these and similar cases, we insert data into the mappings table as a JSON string formatted like this:
```
 source_crm --> {"integrationName":"email","integrationType":"SourceCrm","integrationKey":{"getMethod":"email","postMethod":"email"}}
 target_crm --> {"integrationName":"email","integrationType":"TargetCrm","integrationKey":{"getMethod":["emails","0","value"],"postMethod":"emails"}}
```

- Additionally, for special values such as telephone numbers, we should remove the "+" sign from the beginning of the string before storing it in our database.

<br>

---



### Logic of the integration's process:

We have 2 different process in this addon:

#####1- Sync each action with event and webhooks(for keep the data synced between Source and Target everytime)

In this process, we use events instead of cron jobs to handle synchronization, enhancing productivity. We utilize WPL events for Source CRM, allowing us to sync Create, Update, or Delete actions with Target CRM as they occur. These events are managed for each type of integration (e.g., Follow Up Boss). For details on using triggers, refer to the Triggers document.

<br>

#####2- Sync totally with cronjob (for the first time or whenever we need)

##### Attention:
```
This process is under development and this document just for more knowing about that.
```

Once the cron job is triggered, the sync() method from the app.php file of Target CRM is executed. This method synchronizes data between Source CRM (WPL CRM) and Target CRM (such as FollowUpBoss), supporting actions like create, update, and delete.

    1-1 The first step of synching process is checking the removed items from two sides:
    
        Module's records(like contacts) that are removed from Source CRM (WPL CRM) stored in the crm_integration_remove table.
        if Target CRM, has allowed "delete action", in this step we should remove their records by the rec_ids that get from this table.
        Also, we should get the records from Target CRM, and if they are not exist anymore in the Source CRM, removing them too.<br>
        a- Remove items from Target CRM by check the wpl_addon_crm_integration_remove table for get all rec_ids that removed from Source CRM.<br>
        b- Remove items from Source CRM by check the Target CRM rec_ids that not exist anymore.

    
    1-2 In this step we should complete create action in the synching process:
    
        When an item created in Source CRM, the rec_id is null, so we should get them and create same items by the service_id in the Target CRM.
        But, when an item created in Target CRM, it has been rec_id, so in this step, we should get them and add to the Source CRM.
        a- After get items from Target CRM(by rec_id), we can create them in the Source CRM. 
        b- After get items from Source CRM(rec_id = NULL), we can create them in the Target CRM and update rec_id of exist records in the Source CRM.

    
    1-3 The last step in integration is complete update action by comparing the Source CRM with Target CRM:
    
        If exists any data in Target CRM that it was exist in the Source CRM too, we should first check the item's last update in two sides.
        Then, each side has older date will be update.
        a- Compare data that get from Source CRM with Target CRM and find whichone is up to date. then the Target item can be updated.
        b- Compare data that get from Target CRM with Source CRM and find which record is up to date. then the Source item can be updated by the new one.


---


### How to call cronjob by url:
```
http://localhost/wp/?wpl_crm_integration_cron=1&service=gohighlevel&module=contacts
```