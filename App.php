<?php

namespace Wizban\Core;

use Wizban\Core\Components\Migration\Migration;
use Wizban\Core\Controllers\Backend\ComponentsController;
use Wizban\Core\Controllers\Backend\MainController;
use Wizban\Core\Controllers\OptionsController;
use Wizban\Core\Models\Permission;
use Wizban\Core\Components\Auth\Auth;
use Wizban\Core\Components\Options\Options;
use Wizban\Core\Components\Permissions\Permissions;
use Wizban\Core\Models\Option;
use Wizban\Core\Src\BaseApp;

class App extends BaseApp
{
    /**
     * In this method you can define what you want to run when website is loaded. Of course the plugin should be activated.
     * @return void
     */
    public function init()
    {

        /*
        |--------------------------------------------------------------------------
        | Register the menu
        |--------------------------------------------------------------------------
        */
        add_action('admin_menu', function (){

            add_menu_page(
                __('Wizban Theme', WIZBAN_TRANSLATE_KEY),
                __('Wizban Theme', WIZBAN_TRANSLATE_KEY),
                'manage_options',
                'wizban-theme',
                function (){
                    $obj = new MainController();
                    return $obj->view();
                },
                'dashicons-admin-settings',
                50
            );
        });

        /*
        |--------------------------------------------------------------------------
        | Require the front libraries
        |--------------------------------------------------------------------------
        */
        $this->defineScripts();
    }

    /**
     * We define all routes here that accessible in the wp-json endpoints.
     * @return void
     */
    public function registerRoutes()
    {

        /*
        |--------------------------------------------------------------------------
        | Routes for wp admin dashboard
        |--------------------------------------------------------------------------
        */
        $this->createRoute("GET", "v1", "/backend/options", OptionsController::class, "get", "options_get", true);
        $this->createRoute("POST", "v1", "/backend/options", OptionsController::class, "createOrUpdate", "options_create", true);
        $this->createRoute("DELETE", "v1", "/backend/options", OptionsController::class, "delete", "options_delete", true);
        $this->createRoute("GET", "v1", "/backend/options", OptionsController::class, "get", "options_get", true);
        $this->createRoute("GET", "v1", "/backend/options", OptionsController::class, "get", "options_get", true);
        $this->createRoute("GET", "v1", "/backend/menu", MainController::class, "viewMenuItems", "dashboard_menu_items", true);
        $this->createRoute("GET", "v1", "/backend/components", ComponentsController::class, "get", "components_get", true);
    }

    public function defineScripts()
    {
        $assetPath = WIZBAN_ROOT_ASSETS;

        //Backend loading assets
        add_action('admin_enqueue_scripts', function ($hook) use($assetPath) {

            wp_enqueue_style('wp-dasahboard', $assetPath . 'css/backend/wp-dasahboard.css');
            wp_enqueue_style('backend-style', $assetPath . 'css/backend/backend-style.css');
            wp_enqueue_script('wp-dasahboard', $assetPath . 'js/backend/wp-dashboard.js');
            wp_localize_script('wp-dasahboard', 'wizbanCoreObjectForAdmin', array(
                'rest_url' => esc_url_raw(rest_url()),
                'nonce' => wp_create_nonce('wp_rest'),
                'add_button' => __('Add', WIZBAN_TRANSLATE_KEY),
                'submit_button' => __('Submit', WIZBAN_TRANSLATE_KEY),
                'form_loading' => __('Loading', WIZBAN_TRANSLATE_KEY),
            ));

            if ($hook == "toplevel_page_wizban-theme"){

                wp_enqueue_media();
                wp_enqueue_script('jquery');
                wp_enqueue_style('backend-css', $assetPath . 'backend.min.css');
                wp_enqueue_script('backend-js', $assetPath . 'backend.min.js', array('jquery'), '1.0', ["in_footer" => true]);

                // Localize the script with new data in plugin
                $scriptData = array(
                    'ajax_url' => admin_url('admin-ajax.php'),
                    'rest_url' => esc_url_raw(rest_url()) . $this->namespace . '/v1/',
                    'nonce' => wp_create_nonce('wp_rest'),
                    'plugin_path' => WIZBAN_ROOT_URL,
                    'domain_url' => home_url(),
                    'is_rtl' => is_rtl()
                );

                // Localize the translation for using in js files
                $translation = array(
                    'add_button' => __('Add', WIZBAN_TRANSLATE_KEY),
                    'submit_button' => __('Submit', WIZBAN_TRANSLATE_KEY),
                    'edit_button' => __('Edit', WIZBAN_TRANSLATE_KEY),
                    'remove_button' => __('Remove', WIZBAN_TRANSLATE_KEY),
                    'form_loading' => __('Loading', WIZBAN_TRANSLATE_KEY),
                    'growl_error_title' => __('Error', WIZBAN_TRANSLATE_KEY),
                    'growl_success_title' => __('Successful message', WIZBAN_TRANSLATE_KEY),
                    'growl_error_message_empty' => __('There is nothing to operation.', WIZBAN_TRANSLATE_KEY),
                );

                $scriptData = array_merge($scriptData, $translation);

                wp_localize_script('backend-js', 'wizbanCoreObject', $scriptData);
            }
        });

        //Frontend loading assets
        add_action('wp_enqueue_scripts', function ($hook) use($assetPath) {

            if (!is_admin()) {

                wp_enqueue_style('frontend-css', $assetPath . 'frontend.min.css');
                wp_enqueue_script('frontend-js', $assetPath . 'frontend.min.js', array('jquery'), '1.0', ["in_footer" => false]);

                // Localize the script with new data in plugin
                $scriptData = array(
                    'ajax_url' => admin_url('admin-ajax.php'),
                    'rest_url' => esc_url_raw(rest_url()) . $this->namespace . '/v1/',
                    'nonce' => wp_create_nonce('wp_rest'),
                    'plugin_path' => WIZBAN_ROOT_URL,
                    'is_rtl' => is_rtl()
                );


                // Localize the translation for using in js files
                $translation = array(
                    'growl_success_title' => __('Successful message', WIZBAN_TRANSLATE_KEY),
                    'growl_error_title' => __('Error', WIZBAN_TRANSLATE_KEY),
                    'empty_data' => __('Nothing found.', WIZBAN_TRANSLATE_KEY),
                );

                $scriptData = array_merge($scriptData, $translation);

                wp_localize_script('frontend-js', 'wizbanCoreObject', $scriptData);
            }
        });
    }
}